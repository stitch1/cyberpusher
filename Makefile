bin = .venv/bin
env = env PATH="${bin}:$$PATH"

ifeq ($(shell uname -m),arm64)
env = env PATH="${bin}:$$PATH /usr/bin/arch -x86_64"
else
env = env PATH="${bin}:$$PATH"
endif

venv: .venv/created ## Create virtual environment
.venv/created:
	${MAKE} clean
	python3 -m venv .venv
	. .venv/bin/activate && ${env} pip install -U pip pip-tools
	. .venv/bin/activate && ${env} pip install -Ur requirements.txt
	touch .venv/created


clean:
clean: clean_venv

clean_venv:
	@rm -rf .venv


run: venv
	. .venv/bin/activate && ${env} python3 pusher/manage.py runserver

run-frontend:
	cd pusherfrontend && npm run serve


superuser: venv
	. .venv/bin/activate && ${env} python3 pusher/manage.py createsuperuser

migrate: venv
	. .venv/bin/activate && ${env} python3 pusher/manage.py makemigrations
	. .venv/bin/activate && ${env} python3 pusher/manage.py migrate


pip-compile: ## synchronizes the .venv with the state of requirements.txt
	. .venv/bin/activate && ${env} python3 -m piptools compile requirements.in
	. .venv/bin/activate && ${env} python3 -m piptools compile requirements-dev.in

pip-upgrade: ## synchronizes the .venv with the state of requirements.txt
	. .venv/bin/activate && ${env} python3 -m piptools compile --upgrade requirements.in
	. .venv/bin/activate && ${env} python3 -m piptools compile --upgrade requirements-dev.in

pip-sync: ## synchronizes the .venv with the state of requirements.txt
	. .venv/bin/activate && ${env} python3 -m piptools sync requirements.txt

