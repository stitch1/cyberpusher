import logging
from django.core.management.base import BaseCommand

from pushersoftware.models import CoinInsertion, GameState, PayoutRequest

log = logging.getLogger(__package__)


class Command(BaseCommand):
    help = 'Reset the pusher database.'

    def handle(self, *args, **options):
        CoinInsertion.objects.all().delete()
        GameState.objects.all().delete()
        PayoutRequest.objects.all().delete()

        # Always have a single gamestate ready.
        gs = GameState()
        gs.coin_insertion_counter = 0
        gs.save()
