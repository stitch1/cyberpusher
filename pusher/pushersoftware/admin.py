from django.contrib import admin

# Register your models here.
from django.contrib.admin import ModelAdmin

from pushersoftware.models import CoinInsertion, GameState, PayoutRequest


@admin.register(CoinInsertion)
class CoinInsertionAdmin(admin.ModelAdmin):
    ...


@admin.register(GameState)
class GameStateAdmin(admin.ModelAdmin):
    ...


@admin.register(PayoutRequest)
class PayoutRequestAdmin(admin.ModelAdmin):
    ...
