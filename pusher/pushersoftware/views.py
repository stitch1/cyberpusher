import json
from datetime import timedelta

from django.http import JsonResponse
from django.utils import timezone
from pushersoftware.models import CoinInsertion, GameState, PayoutRequest
import channels.layers
from asgiref.sync import async_to_sync
import logging
log = logging.getLogger(__name__)

"""
The hopper pays out an extra coin every 20/40 payouts. This means that the "behind schedule"
feature is obsolete for as long as we don't register (and within time) handle the feedback from the hopper.
That perhaps needs it's own arduino to make sure the timing and feedback is correct (and nothing distracts)
"""


STEADY_PAYOUT_PER_SECONDS = 60
EXTRA_PAYOUT_IF_LOWER_THAN_N_COINS_IN_TIMESPAN = 60  # 60 means, 60 coins payed out in the last hour when timespan=3600.
EXTRA_PAYOUT_TIMESPAN = 3600  # one hour
BEHIND_SCHEDULE_PAYOUT_PER_INSERTS = 1000  # a ridiculously high number basically disables this feature, but still
# makes it exploitable for the best of trolls :)


def setup_live(request):
    setup()
    return JsonResponse({})


def setup():
    log.debug("Setting up")
    if not GameState.objects.all().count():
        gs = GameState()
        gs.coin_insertion_counter = 0
        gs.save()


def insert_coin_live(request):
    should_payout_prize()
    return insert_coin()


def broadcast(message: str):
    log.debug("Broadcasting message: %s", message)
    # https://channels.readthedocs.io/en/latest/tutorial/part_2.html
    # https://stackoverflow.com/questions/53461830/send-message-using-django-channels-from-outside-consumer-class
    channel_layer = channels.layers.get_channel_layer()
    async_to_sync(channel_layer.group_send)(
        'chat_lobby',
        {
            'type': 'chat_message',
            'message': message
        }
    )


def insert_coin():
    insertion = CoinInsertion()
    insertion.at_when = timezone.now()
    insertion.save()
    log.debug("Coin Insertion Stored")

    counter = GameState.objects.all().first()
    counter.coin_insertion_counter += 1
    counter.save()
    log.debug("Coin insertion counter increased to %s", counter.coin_insertion_counter)

    state = create_gamestate()
    state['message'] = "inserted coin"
    state['type'] = "gamestate"
    broadcast(json.dumps(state))

    return JsonResponse(state)


def should_payout_prize_live(request):
    return should_payout_prize()


def deterimine_if_it_is_time_for_payout():
    # prize_payed_out_in_the_last_minute
    if not PayoutRequest.objects.all().filter(
            at_when__gte=timezone.now() - timedelta(seconds=STEADY_PAYOUT_PER_SECONDS),
            reason="payout_time_reached").count():
        p = PayoutRequest()
        p.at_when = timezone.now()
        p.has_been_payed_out = False
        counter = GameState.objects.all().first()
        p.amount_of_coins_inserted_now = counter.coin_insertion_counter
        p.reason = "payout_time_reached"
        p.save()
        log.debug("Added payout_time_reached payout request.")

    # behind schedule, we don't care for the reason
    if PayoutRequest.objects.all().filter(
            at_when__gte=timezone.now() - timedelta(seconds=EXTRA_PAYOUT_TIMESPAN)
    ).count() < EXTRA_PAYOUT_IF_LOWER_THAN_N_COINS_IN_TIMESPAN:

        # see if enough coins are inserted to trigger an extra payout.
        counter = GameState.objects.all().first()
        if (counter.coin_insertion_counter / BEHIND_SCHEDULE_PAYOUT_PER_INSERTS).is_integer():
            p = PayoutRequest()
            p.at_when = timezone.now()
            counter = GameState.objects.all().first()
            p.amount_of_coins_inserted_now = counter.coin_insertion_counter
            p.has_been_payed_out = False
            p.reason = "behind_schedule"
            p.save()
        log.debug("Added behind-schedule payout request.")


def should_payout_prize():
    """
    There are 250 coins in the hopper. You need 2 coins to win a prize = 125 prizes per hopper.
    Every day we give away 500 prizes: requiring emptying the hopper 4 times.

    Payout happens once every 60 seconds, which means it takes 250 minutes to empty the hopper (=4 hours).
    So we need to refill the hopper 4 times a day, every four hours with intensive play.

    If the coins are not awared in this timeslot, an extra prize is awarded every 30 coins inserted.
    This routine calculates if a prize should be awarded, and reserves them in the payout table.

    The payout table is then read, and if there is a prize to pay out, this routine says "yes".

    :param request:
    :return:
    """
    deterimine_if_it_is_time_for_payout()
    return payout_one_open_payout_request()


def payout_one_open_payout_request():
    # See if there are are any open prizes
    to_payout = PayoutRequest.objects.all().filter(has_been_payed_out=False).first()
    if not to_payout:
        log.debug("No open payout requests")
        return False

    to_payout.has_been_payed_out = True
    to_payout.payed_out_on = timezone.now()
    to_payout.save()

    state = create_gamestate()
    state['message'] = "payout"
    state['type'] = "gamestate"
    log.debug("Paying out")

    broadcast(json.dumps(state))

    return True


def create_gamestate():
    coins_in_last_60_seconds = load_seconds(60)
    counter = GameState.objects.all().first()
    payouts = PayoutRequest.objects.all().filter(has_been_payed_out=True).count()

    return {
        'coins_in_last_60_seconds': coins_in_last_60_seconds,
        'average_coins_in_list_60_seconds': round(coins_in_last_60_seconds / 60, 1),
        'total_coin_inserts': counter.coin_insertion_counter,
        'payouts': payouts
    }


def load_seconds(seconds):
    return CoinInsertion.objects.all().filter(at_when__gte=timezone.now() - timedelta(seconds=seconds)).count()

