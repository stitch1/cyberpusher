from django.db import models


class CoinInsertion(models.Model):
    at_when = models.DateTimeField()

    def __str__(self):
        return self.at_when


class GameState(models.Model):
    coin_insertion_counter = models.BigIntegerField(
        help_text="Amount of coins inserted, used to calculate extra prizes. Saves counting records in CoinInsertion."
    )

    def __str__(self):
        return self.coin_insertion_counter


class PayoutRequest(models.Model):
    at_when = models.DateTimeField()

    payed_out_on = models.DateTimeField(
        null=True,
        blank=True
    )

    """
    The algorithm is simple: did we pay out 60 coins in the last 60 minutes? If not, pay out a coin per 20 inserts.
    """
    reason = models.CharField(
        max_length=240,
        help_text="There can be several reasons a payout is requested: behind schedule or payout time reached.",
        default=""
    )

    has_been_payed_out = models.BooleanField(
        help_text="Multiple payouts can be registered, every coin insert this is checked. If something is not "
                  "payout out, it will pay out. It's a bit like 'tickets owed' in arcade machines. It survives reboots "
                  "and such."
    )

    amount_of_coins_inserted_now = models.BigIntegerField(
        help_text="",
        default=0
    )

    def __str__(self):
        return f"{self.at_when} {self.reason}"
