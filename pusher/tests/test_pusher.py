"""
Invoke from the project root with: python -m pytest -v tests -k test_pusher

"""
import time
from datetime import timedelta

import pytest
from django.utils import timezone
from pushersoftware.views import insert_coin, should_payout_prize, setup

from pushersoftware.models import CoinInsertion, GameState


def test_insert_coin(db) -> None:
    setup()
    insert_coin()
    insert_coin()
    insert_coin()
    insert_coin()
    insert_coin()

    assert CoinInsertion.objects.all().count() == 5

    first_gamestate = GameState.objects.all().first()
    assert first_gamestate.coin_insertion_counter == 5

    insert_coins(20)
    assert CoinInsertion.objects.all().count() == 25

    first_gamestate = GameState.objects.all().first()
    assert first_gamestate.coin_insertion_counter == 25


def test_should_payout_prize(db) -> None:
    setup()
    insert_coin()

    # first call, nothing before, should always return true
    assert True is should_payout_prize()

    # nothing inserted in the machine, but the last 60 minutes
    # nothing was payed out, so will register a payout, but will
    # not pay out unless a specific number of coins has been inserted
    assert False is should_payout_prize()

    # A lot more payouts will be registered if we call should_payout,
    # because nothing happened in the last 60 minutes.
    # Let's insert the number of coins we need
    # we should be at 20 coins now, and this means a payout:
    insert_coins(19)
    assert True is should_payout_prize()

    # the payout should not happen again, we're always calling
    # insert_coin() and then should_payout_prize():
    # todo: what if we keep calling should_payout_prize: that should not payout if the number
    #  of coins has not risen.
    insert_coin()
    assert False is should_payout_prize()


def test_load(db) -> None:
    # At 21 dec the max per minute input is 388 coins via the command line.
    # That's about 6.5 coins per second. Good luck :)

    # todo: test this on the py itself, it will be slower than my trusty laptop
    # 100.000 inserts takes about 2.38. An insert is about 0.002 seconds. Way faster than needed.
    # todo: we'll have to figure out what the load is if we also have a browser running
    # with animation...
    # due to the 2 minutes waiting time, the load test had been reduces to 100 entries.
    insert_coins(100)

    # An insert coin should still be very fast after 100.000 inserts:
    then = timezone.now()
    insert_coin()
    duration = timezone.now() - then
    assert duration < timedelta(seconds=0.05)

    # This test should pass in less than half a second.
    then = timezone.now()
    should_payout_prize()
    should_payout_prize()
    should_payout_prize()
    should_payout_prize()
    should_payout_prize()
    duration = timezone.now() - then

    # Even with a gigantic amount of coins, the app should still be fast.
    # A four day event is 345600 seconds. So with one coin a second, it should still be fast
    # and we should able to handle an add second after 100.000 k seconds still very fast.
    assert duration < timedelta(seconds=0.05)


def insert_coins(amount: int):
    while amount > 0:
        insert_coin()
        amount -= 1
