#!/bin/bash
# Run as: sudo configure_raspian.sh
# Get this script: wget https://gitlab.com/stitch1/cyberpusher/blob/master/configure_raspian.sh
# Then make it executable: chmod +x configure_raspian.sh
# Then run it as root: sudo configure_raspian.sh

# Raspian, a non-declarative OS that requires tons of installation and configuration by hand and is brittle as ...
# This makes me wish that all OS-es are declarative. It would save massive amounts of time.

# This script installs all dependencies and other nightmare packages to get the system up and running.
# It's written in shell, so we'll never get rid of this language.

# Enable SSH on the machine by default, so we can do remote administration:
sudo systemctl enable ssh
sudo systemctl start ssh

# Manually configure the top panel to not show this is done in the last tab. 0 pixels also.
# There is already a chromium on the machine, so perhaps we can skip firefox.

# Change the password for the pi user:
passwd


# END PRE SCRIPT


# stop on errors
# http://redsymbol.net/articles/unofficial-bash-strict-mode/
set -euo pipefail
IFS=$'\n\t'

next-installation-step () {
  cd ~
  echo ""
  echo "-----------------------------------------------------"
  echo "|Installation: $1"
  echo "-----------------------------------------------------"
}




next-installation-step "Install the display drivers for the 5 inch screen"
# This will fail under debian-test-installations, due to the /pi/ user missing etc.
# https://www.waveshare.com/wiki/5inch_HDMI_LCD
# apt install git -y -> git is already included
cat >/config.txt <<EOL
max_usb_current=1
hdmi_group=2
hdmi_mode=87
hdmi_cvt 800 480 60 6 0 0 0
hdmi_drive=1
EOL
# OFC the malware is half-supported: https://github.com/goodtft/LCD-show/issues/159
rm -rf LCD-show  # Prevent existing dirs to interfere
git clone https://github.com/waveshare/LCD-show.git
cd LCD-show/
chmod +x LCD5-show
# Change the LCD5-show script to NOT reboot, because it's breaking the 'simple' installation.
sed -i 's/sudo reboot//g' LCD5-show
./LCD5-show # acoording to https://github.com/goodtft/LCD-show/issues/166#issuecomment-558626138 this works on buster
# ./MHS35-show


next-installation-step "Make sure that the display never turns black when there is no input."
# See: http://www.etcwiki.org/wiki/Disable_screensaver_and_screen_blanking_Raspberry_Pi
# Does not work: Open this file: /etc/xdg/lxsession/LXDE-pi/autostart
# Does not work:Add the following lines:
# Does not work:@xset s noblank
# Does not work:@xset s off
# Does not work:@xset -dpms
# Instead set up xscreensaver and then disabled via the settings menu, because discoverability is for idiots.
apt install xscreensaver
# You have to type in xscreensaver afterwards, and set "disable screensaver completely"

# First do the user-interaction stuff...
# Create the pusher user, with the pusher password
next-installation-step "First we'll do all user-interaction stuff."
# https://www.digitalocean.com/community/tutorials/how-to-add-and-delete-users-on-debian-8
adduser pusher  # you'll have to enter the password and info. The python script runs under this user.


next-installation-step "User interaction not required from here."
# Run this script as root on a new device.

next-installation-step "Updating raspian software package list, so we will never know what we'll be installing exactly."
apt update


# next-installation-step "Installing net-tools, for old school network maintenance with ifconfig and netstat."
# apt-get install net-tools -y -> already shipped


# there already is a chromium installation, so perhaps we can just skip this.
# And have chrmoium kiosk mode (if that is still a thing) starting full screen on boot.
# next-installation-step "Installing firefox-esr to show the current score and other info on the display."
# apt install firefox-esr -y


next-installation-step "Install pusher dependencies: Redis server for websocket support"
# https://computingforgeeks.com/how-to-install-redis-on-debian/
apt install redis-server -y  # For async operations on the web interface
# Run it at startup:
sudo systemctl enable --now redis-server.service


next-installation-step "Install pusher dependencies: SDL for audio via the pygame pip package"
# pygame is NOT compattible with python 3.8, so we're getting 3.7
# This is default on a raspian installation, but we still need to see if the other python deps work.
# So we'll just install it. If it's already there, no problemo :)
# https://wiki.libsdl.org/Installation#Linux.2FUnix
# You need SDL 1: https://raspberrypi.stackexchange.com/questions/66418/unable-to-run-sdl-config
# apt install libsdl2-2.0 -y  -> already installed
# apt install libsdl2-dev -y -> already installed
# apt install libsdl-dev -y -> already installed
# freetype is already available.
# apt install python-pygame -y -> already installed
# python3-pygame does not exist yet?



# Python 3.7.3 is co-shipped with raspian. So that's good enough.
:' next-installation-step "Installing Python3.7, so we have neater syntax to work with."
# pygame is NOT compattible with python 3.8, so were getting 3.7
# The 12 easy install steps everybody will know intuitively by following their heart:
# https://installvirtual.com/how-to-install-python-3-8-on-raspberry-pi-raspbian/
apt-get install -y build-essential tk-dev libncurses5-dev libncursesw5-dev libreadline6-dev libdb5.3-dev libgdbm-dev libsqlite3-dev libssl-dev libbz2-dev libexpat1-dev liblzma-dev zlib1g-dev libffi-dev tar wget vim -y
wget https://www.python.org/ftp/python/3.7.0/Python-3.7.0.tgz
tar zxf Python-3.7.0.tgz
cd Python-3.7.0
./configure --enable-optimizations
make -j 4
make altinstall
python3.7 -V
echo "alias python='/usr/local/bin/python3.8'" >> /root/.bashrc
echo "alias python='/usr/local/bin/python3.8'" >> /home/pi/.bashrc
# they mean, reload the file...
source /root/.bashrc
python3.7 --version
'


next-installation-step "Install python packages for the pusher"
# You'll have to upload the project locally using deploy_over_ssh (due to the music/sound library)
wget https://gitlab.com/stitch1/cyberpusher/raw/master/requirements.txt
# Get all relevant python modules
python3 -m pip install -r ~/requirements.txt


next-installation-step "Download the software, which might save some network config time and such."
git clone https://gitlab.com/stitch1/cyberpusher/

# Install the pusher service at the right place
# See: https://wiki.debian.org/systemd/Services
cp /home/pi/cyberpusher/pusher.service /etc/systemd/system
chmod +x /home/pi/cyberpusher/start_pusher.sh
chmod +x /home/pi/cyberpusher/stop_pusher.sh
systemctl daemon-reload
systemctl enable pusher.service
systemctl start pusher.service
systemctl status pusher



# done: Start the app -> see start pusher

# Start the browser in kiosk mode, done: make a service, impossibru to do so in a way that i understand
# see for service: https://raspberrypi.stackexchange.com/questions/104827/start-chromium-on-raspberry-pi-in-kiosk-mode-using-systemd-service-instead-of-th
# over SSH you need to first do: export DISPLAY=:0.0
/usr/bin/chromium-browser --noerrdialogs --disable-infobars --kiosk http://localhost:8080 &



# remove the cursor after a moment of inactivity, which gives a cleaner look, it removes the mouse after a few seconds
apt install unclutter -y
# alternative: https://raspberrypi.stackexchange.com/questions/53127/how-to-permanently-hide-mouse-pointer-or-cursor-on-raspberry-pi


next-installation-step "Set up wired ethernet to only work for certain IP addresses"
# set up ethernet to a fixed ip, will only work with wired ethernet: 100.66.6.42
# https://linuxconfig.org/how-to-set-a-static-ip-address-on-debian-10-buster
# no internet for you, only via another computer
systemctl stop NetworkManager
systemctl disable NetworkManager
apt install resolvconf -y

# Todo: automate this:
# in your /etc/dhcpcd.conf, add this:
# interface eth0
# static ip_address=100.66.6.42

# If you need wifi, which you don't during the event:
# interface wlan0
# static ip_address=192.168.2.200/24
# static routers=192.168.2.254
# static domain_name_servers=192.168.2.254

# The below code will fuck up your network config, and it will remove all your network devices.
# An OS that allows that type of system destruction is plain evil.
# cat > /etc/network/interfaces <<EOL
# iface eth0 inet static
#     address 100.66.6.42
#    broadcast 100.66.6.255
#     netmask 255.255.255.0
# EOL
# no gateway needed.x
# no DNS needed
systemctl restart networking
systemctl restart resolvconf


next-installation-step "Disable bluetooth and wifi"
# https://blog.sleeplessbeastie.eu/2018/12/31/how-to-disable-onboard-wifi-and-bluetooth-on-raspberry-pi-3/
echo "dtoverlay=pi3-disable-wifi" | sudo tee -a /boot/config.txt
echo "dtoverlay=pi3-disable-bt" | sudo tee -a /boot/config.txt
systemctl disable hciuart


# now reboot for lcd5-show and such
next-installation-step "Do you want to reboot?"
# https://stackoverflow.com/questions/1885525/how-do-i-prompt-a-user-for-confirmation-in-bash-script
read -p "Do you want to reboot? " -n 1 -r
echo    # (optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then
    # do dangerous stuff
    sudo reboot
fi



# apt install npm
# sudo apt-get install nodejs npm node-semver

# install emoji font for confetti in UI:
# sudo apt install fonts-noto-color-emoji