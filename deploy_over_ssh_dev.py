"""
All these commands are performed by the pusher user, created by the configure_raspian.sh script. Make sure
your raspian is configured with that script.
"""
import pexpect
from paramiko import SSHClient
import paramiko

from deploy_config import DEPLOY_CONFIG

# Because pip does not deliver a version where there are no crypto warnings.
import warnings

from deploy_over_ssh import perform_ssh_command, publish_software_over_ssh

warnings.filterwarnings(action='ignore',module='.*paramiko.*')

ip = DEPLOY_CONFIG['ip']
username = DEPLOY_CONFIG['username']
password = DEPLOY_CONFIG['password']

if __name__ == '__main__':
    perform_ssh_command("/home/pi/cyberpusher/stop_pusher.sh")
    publish_software_over_ssh()
    print("Dev update complete")
