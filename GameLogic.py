import json
import os
import platform
import random
from pathlib import Path
import logging
import threading
import pygame.mixer
import time
import requests
from datetime import datetime

logging.basicConfig(level=logging.DEBUG)
log = logging

INSTALL_PATH = Path(__file__).parent


class GameLogic:
    """
    This is a class because a lot of variables had to be imported with 'global' in functions, which looked like
    an anti-pattern.
    """
    settings = {
        "initial_design": "pro",
    }

    environment = {
        "possible_designs": [],
    }

    soundsets = {
        # music is a different thing in pygame, which understands most MP3 files, next to WAV.
        'music': {"volume": 0.4, "playlist": []},

        # every time a coin is inserted
        'coin_inserted': {'volume': 0.9, 'playlist': []},
        'boot': {'volume': 0.9, 'playlist': []},

        # everytime a prize is payed out
        'payout': {'volume': 0.9, 'playlist': []},
        'pre_payout': {'volume': 0.8, 'playlist': []},
    }

    now_playing = ""

    def __init__(self):
        self.initialize_audio()
        self.load_settings()
        self.load_environment()
        self.load_design()
        self.boot()

    def initialize_audio(self):
        # very little lag, coin insert sounds don't overlay otherwise due to too large resolutions
        # TypeError: 'buffersize' is an invalid keyword argument for this function
        # on mac mp3 files do not play...
        # ffmpeg -i Plastic\ Dreams\ -\ Jaydee\ ORIGINAL_PN.mp3 -acodec pcm_u8 -ar 44100 Plastic\
        # Dreams\ -\ Jaydee\ ORIGINAL_PN.wav

        pygame.mixer.pre_init(frequency=44100, size=-16, channels=2, buffer=2048)  # noqa (it is buffer, not buffersize(!))
        # results in: # ALSA lib pcm.c:8306:(snd_pcm_recover) underrun occurred -> buffer was too small
        # python3 -m pip install -U pygame --user
        pygame.mixer.init(frequency=44100)

        # setup audio
        pygame.mixer.set_num_channels(30)

    def load_settings(self):
        with open('%s/settings.json' % INSTALL_PATH) as f:
            settings = json.load(f)
            self.settings['initial_design'] = settings["initial_design"]

    def load_environment(self):
        self.environment['possible_designs'] = [f for f in os.listdir("%s/designs" % INSTALL_PATH) if not f.startswith('.')]

    def load_design(self):
        # You can have your own designs for sounds, music and more if that's available later...

        if self.settings['initial_design'] not in self.environment['possible_designs']:
            raise ReferenceError("Design %s does not exist in possible designs %s." % (
                self.settings['initial_design'], self.environment['possible_designs']))

        soundsets = self.soundsets.keys()
        for soundset in soundsets:
            self.soundsets[soundset]['playlist'] = self.load_soundset_files(soundset)

    def load_soundset_files(self, soundset):
        log.debug("Loading sounds from soundset %s." % soundset)
        design_path = "%s/designs/%s" % (INSTALL_PATH, self.settings['initial_design'])

        # Just raise the FileNotFoundError if the files do not exist, then we can debug what is going on.
        files = ["%s/%s/%s" % (design_path, soundset, f) for f in os.listdir("%s/%s/" % (design_path, soundset)) if not f.startswith('.')]

        log.debug("Soundset %s contains %s files" % (soundset, len(files)))
        random.shuffle(files)  # inline shuffling.
        return files

    def boot(self):
        self.sound_play_random("boot")
        self.just_play_music_all_the_time()

    def quit(self):
        pygame.mixer.music.stop()
        pygame.mixer.quit()
        pygame.quit()
        log.debug("Stopping")
        exit(0)

    def sound_play_random(self, soundset):
        try:
            if not self.soundsets[soundset]["playlist"]:
                return

            next_sound = random.choice(self.soundsets[soundset]["playlist"])

            sound = pygame.mixer.Sound(next_sound)
            sound.set_volume(self.soundsets[soundset]["volume"])

            chan = pygame.mixer.find_channel()
            chan.play(sound)
        except IndexError:
            log.error("No  sounds in soundset %s" % soundset)

    def just_play_music_all_the_time(self):

        # On mac, getting pygame to work properly is impossible, all tracks glitch and cause massive hangs.
        # So we're not supporting music on mac (or anything other platform that pigame sort of works on)
        if not platform.system().startswith('Linux'):
            return

        def music():
            while True:
                time.sleep(0.2)
                self.music_try_next()

        try:
            thr = threading.Thread(target=music, args=(), kwargs={})
            thr.start()  #
        except KeyboardInterrupt:
            pass

    def music_try_next(self, sequential: bool = False):
        """

        :param sequential: Force the next track in the array, otherwise a random choice is made. This is helpful
        for debugging, as pygame sometimes has issues playing certain songs (it plays them at 5% speed or so, while
        all data for the song is correct). This allows pusher_dev to figure out what is being played and such.
        :return:
        """
        if pygame.mixer.music.get_busy():
            return

        # no music?
        if not self.soundsets["music"]["playlist"]:
            return

        if not sequential:
            next_track = random.choice(self.soundsets["music"]["playlist"])
        else:
            if not self.now_playing:
                next_track = self.soundsets["music"]["playlist"][0]
            else:
                # loop around the entire playlist, if we reach the end, start over.
                current_track = self.soundsets["music"]["playlist"].index(self.now_playing)
                if len(self.soundsets["music"]["playlist"]) > (current_track + 1):
                    next_track = self.soundsets["music"]["playlist"][current_track + 1]
                else:
                    next_track = self.soundsets["music"]["playlist"][0]

        self.now_playing = next_track

        track_id = self.soundsets["music"]["playlist"].index(self.now_playing)
        # pygame crashes on some songs on mac, and we need to figure out which ones
        log.debug(f"Playing song {track_id}: {next_track}")
        pygame.mixer.music.load(next_track)
        pygame.mixer.music.set_volume(self.soundsets["music"]["volume"])
        pygame.mixer.music.play()

    def music_force_next(self):
        self.music_stop()
        self.music_try_next(sequential=True)

    def music_mute(self):
        pygame.mixer.music.set_volume(0.0)

    def music_stop(self):
        pygame.mixer.music.stop()

    """
    The actual game logic of the pusher, which handles bootups, excrement of coins levels, etc
    """
    def event_insert_coin(self):
        self.handle_insert_coin()

    def handle_insert_coin(self):
        log.debug("Coin Inserted: %s" + str(datetime.now()))
        requests.get("http://localhost:8000/insert_coin/")
        self.sound_play_random("coin_inserted")

    def handle_should_payout_prize(self):
        response = requests.get("http://localhost:8000/should_payout_prize/")
        response = response.json()
        return response['payout']

    def handle_payout_prize(self):
        self.sound_play_random("pre_payout")

        # https://www.python-course.eu/python3_lambda.php
        # https://docs.python.org/3/library/threading.html
        t = threading.Timer(1.0, lambda: self.sound_play_random("payout"))
        t.start()
