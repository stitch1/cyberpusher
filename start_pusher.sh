#!/bin/bash

# This pops up in journalctl
echo "Starting the pusher!"

# Support SSH startups, which need a display to start things on
export DISPLAY=:0.0

# https://stackoverflow.com/questions/16807876/how-to-check-if-another-instance-of-my-shell-script-is-running
for pid in $(pidof -x start_pusher.sh); do
    if [ $pid != $$ ]; then
        echo "[$(date)] : start_pusher.sh : Process is already running with PID $pid"
        exit 1
    fi
done

# First check that we're not already running:
# if 2 -gt 2 is false... I want to stab out my eyeballs because of this dysfunction language.
# n=2
# if [ "$(ps -aux | grep -c start_pusher.sh)" -gt "$n" ]
# then
#   ps -aux | grep -c start_pusher.sh
#   echo "Pusher already starting, not starting again."
#   exit 1
# fi

if [ "$(ps -aux | grep -c pusher_live.py)" -gt 1 ]
then
  echo "Pusher Live: Pusher already running, stop pusher first if you want to run again."
  exit 1
fi

if [ "$(ps -aux | grep -c manage.py)" -gt 1 ]
then
  echo "Manage.py: Pusher already running, stop pusher first if you want to run again."
  exit 1
fi

if [ "$(ps -aux | grep -c chromium-browser)" -gt 1 ]
then
  echo "Chromium-browser: Pusher already running, stop pusher first if you want to run again."
  exit 1
fi


echo "Starting pusher hardware / raspi"
/usr/bin/python3 /home/pi/cyberpusher/pusher_live.py &

# start django web interface
echo "Starting Django"
/usr/bin/python3 /home/pi/cyberpusher/pusher/manage.py runserver &

echo "Starting NPM"
cd /home/pi/cyberpusher/pusherfrontend/ && npm run serve &

# When starting over SSH: export DISPLAY=:0.0
# --app removes the "restore pages" message.
# Wait with booting this, becuase the server has to be running.
echo "Starting browser in 30 seconds."
sleep 20
/usr/bin/chromium-browser --noerrdialogs --disable-infobars --disable-session-crashed-bubble --disable-restore-session-state --kiosk --app=http://localhost:8080 &


echo "Pusher start complete. Chromium and python3 should be running in the background."
