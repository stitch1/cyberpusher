#!/usr/bin/env python3

try:
    import RPi.GPIO as GPIO
except ImportError:
    """
    import FakeRPi.GPIO as GPIO
    OR
    import FakeRPi.RPiO as RPiO
    """
    import FakeRPi.GPIO as GPIO

import time
from GameLogic import GameLogic

"""
    J8
                           .___.              
                  +3V3---1-|O O|--2--+5V
          (SDA)  GPIO2---3-|O O|--4--+5V
         (SCL1)  GPIO3---5-|O O|--6--_
    (GPIO_GLCK)  GPIO4---7-|O O|--8-----GPIO14 (TXD0)
                      _--9-|O.O|-10-----GPIO15 (RXD0)
    (GPIO_GEN0) GPIO17--11-|O O|-12-----GPIO18 (GPIO_GEN1)
    (GPIO_GEN2) GPIO27--13-|O O|-14--_
    (GPIO_GEN3) GPIO22--15-|O O|-16-----GPIO23 (GPIO_GEN4)
                  +3V3--17-|O O|-18-----GPIO24 (GPIO_GEN5)
     (SPI_MOSI) GPIO10--19-|O.O|-20--_
     (SPI_MOSO) GPIO9 --21-|O O|-22-----GPIO25 (GPIO_GEN6)
     (SPI_SCLK) GPIO11--23-|O O|-24-----GPIO8  (SPI_C0_N)
                      _-25-|O O|-26-----GPIO7  (SPI_C1_N)
       (EEPROM) ID_SD---27-|O O|-28-----ID_SC Reserved for ID EEPROM
                GPIO5---29-|O.O|-30--_
                GPIO6---31-|O O|-32-----GPIO12
                GPIO13--33-|O O|-34--_
                GPIO19--35-|O O|-36-----GPIO16
                GPIO26--37-|O O|-38-----GPIO20
                      _-39-|O O|-40-----GPIO21
                           '---'
                       40W 0.1" PIN HDR
"""

# todo: add pusher payout spooler, so there are no double payout events.

# https://www.raspberrypi.org/documentation/usage/gpio/README.md
# https://github.com/splitbrain/rpibplusleafv
SensorGPIO_OLD = 23  # pin 16 # This GPIO just stopped working after 4 days and a reboot. so wtf.
HopperGPIO_OLD = 24  # pin 18

SensorGPIO = 22  # pin 15
HopperGPIO = 5  # pin 29



def test_hardware():
    setup_hardware()
    # 0.15 = 1 coins
    # 0.2 = 2 coins
    # 0.3 = 2 coins
    # 0.4 = 4 coins
    # 0.5 = 5 coins

    # 0.15 = 2 coins per 3/4 payouts.
    # 0.12 = 1/20 to 1/40 are double payouts.
    # 0.11 = 1/20 to 1/30 are NO payouts.
    pay_one_coin = 0.12

    print("Testing hopper: durations from 0.1 to 0.5 seconds.")
    for i in range(0, 30):
        turn_hopper_on_for(pay_one_coin)  # 1 coin

    # turn_hopper_on_for(0.2)  # 2 coins
    # turn_hopper_on_for(0.3)  # 2 coins
    # turn_hopper_on_for(0.4)  # 3 coins
    # turn_hopper_on_for(0.5)  # 4 coins


def turn_hopper_on_for(duration: float = 0.5):
    GPIO.output(HopperGPIO, GPIO.HIGH)
    time.sleep(duration)
    GPIO.output(HopperGPIO, GPIO.LOW)
    time.sleep(1)


def setup_hardware():
    # Tripwire met laser module en lichtsensor
    # https://raspberrytips.nl/tripwire-laser
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)

    # hopper niet uitbetalen
    GPIO.setup(HopperGPIO, GPIO.OUT)
    GPIO.output(HopperGPIO, GPIO.LOW)

    # setup de lichtsensor
    GPIO.setup(SensorGPIO_OLD, GPIO.IN)
    GPIO.setup(SensorGPIO, GPIO.IN)
    GPIO.add_event_detect(SensorGPIO, GPIO.RISING, callback=laser_tripped, bouncetime=200)


def laser_tripped(channel):
    if GPIO.input(channel):
        logic.event_insert_coin()
        if logic.handle_should_payout_prize():
            logic.handle_payout_prize()
            run_hopper()


def run_hopper():
    # enables the hopper to pay out a single coin (approx, it's a time thing)
    # and we don't have pulses yet.

    # Does it work like this?
    GPIO.output(HopperGPIO, GPIO.HIGH)

    # 0.5 is about 3 coins each time.
    # We don't read out the pin on the hopper, it's just a sort of fixed duration
    # that will at least drop one coin on the field. It's ok if it are two sometimes.
    time.sleep(0.12)
    GPIO.output(HopperGPIO, GPIO.LOW)
    pass


if __name__ == '__main__':

    setup_hardware()
    logic = GameLogic()

    try:
        time.sleep(0.2)
    except KeyboardInterrupt:
        print("Received keyboardinterrupt. Quitting...")
        GPIO.remove_event_detect(SensorGPIO)
        GPIO.cleanup()
