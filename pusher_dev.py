from GameLogic import GameLogic
import time


def testloop(logic):
    while True:
        print("""
        1: insert coin
        2: insert 25 coins fast

        d: Specify another design
        n: next song in music_playlist (sequential)
        m: mute music

        q: quit
        exit: quit
        """)
        choice = None

        try:
            choice = input("Your choice: ")
        except KeyboardInterrupt:
            print("Ok Bye")
            exit()

        if choice == "1":
            logic.event_insert_coin()

        if choice == "p":
            logic.handle_payout_prize()

        if choice == "2":
            for i in range(0, 25):
                logic.event_insert_coin()
                time.sleep(0.1)

        if choice == 'm':
            logic.music_mute()

        if choice == "n":
            logic.music_force_next()

        if choice == "exit" or choice == 'q':
            exit()

# load something at startup
try:
    logic = GameLogic()

    testloop(logic)
    # the idea is to create a webpage that shows the state of the game.
    # start_webserver()
except KeyboardInterrupt:
    print("CTRL-C received: quitting...")
    quit()


