"""
All these commands are performed by the pusher user, created by the configure_raspian.sh script. Make sure
your raspian is configured with that script.
"""
import pexpect
from paramiko import SSHClient
import paramiko

from deploy_config import DEPLOY_CONFIG

# Because pip does not deliver a version where there are no crypto warnings.
import warnings
warnings.filterwarnings(action='ignore',module='.*paramiko.*')

ip = DEPLOY_CONFIG['ip']
username = DEPLOY_CONFIG['username']
password = DEPLOY_CONFIG['password']
# todo: verify ssh key, we care little for now.
# todo: update services after deploy, set permissions


def perform_ssh_command(command):
    client = SSHClient()
    print(f"Performing command {command}")
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    client.connect(ip, username=username, password=password)
    stdin2, stdout2, stderr2 = client.exec_command(command)
    print(f'\nOut: {stdout2}\nError: {stderr2}')
    # If you don't readout with paramiko, then you'll NOT run the command. That's how it works. Deal with it.
    z = stdout2.read()
    print(z)
    client.close()
    print("\n")


def publish_software_over_ssh():
    # this requires rsync on your part.
    # https://github.com/pexpect/pexpect/blob/master/doc/overview.rst
    print("Updating software")
    print(f"Rsyncing...")
    args = ["-c", f"rsync --progress --archive --verbose --exclude 'deploy_config.py' --delete . {username}@{ip}:/home/pi/cyberpusher"]
    print(f"Performing command: {args}")
    child = pexpect.spawn('/bin/bash', args=args, timeout=120)
    child.expect(f"{username}@{ip}'s password: ")
    child.sendline(password)
    # wait until finished
    child.expect(pexpect.EOF)
    print("\n")


if __name__ == '__main__':
    print("\n\n")
    print("This script deploys the currently available pusher software over SSH to your pi.")
    print("Make sure your device is connected.")

    perform_ssh_command("/home/pi/cyberpusher/stop_pusher.sh")
    publish_software_over_ssh()
    perform_ssh_command("/home/pi/cyberpusher/start_pusher.sh")
    print("Update complete")
