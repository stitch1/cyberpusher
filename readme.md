# Installation
Written for mac users.

To get all the installation files on your machine:
git clone https://gitlab.com/stitch1/cyberpusher

This is NOT an easy installation, prepare to spend some time installing and upgrading.


## Installing the pi with a clean OS
You need: a micro SD card of at least 8 gb, be able to write micro SD cards

* Download raspian buster (only tested with buster): https://www.raspberrypi.org/downloads/raspbian/
* Unzip the downloaded file, a .img file appears
* Download the etcher tool: https://github.com/balena-io/etcher/
* Write the img file to the SD card
* Insert the micro SD card in the pi and turn it on

## Configuring the clean OS
You need: a keyboard, display and mouse connected to the raspberry pi

You need to log in over SSH on the pi. After booting you'll get a nice UI.
Todo: what do you need to do to enable ssh?

After SSH is enabled, you need to run configure_raspian.sh. You can download this file from the net
using this command:

`bash -e "$(curl -fsSL https://gitlab.com/stitch1/cyberpusher/raw/master/configure_raspian.sh)"`

You will be asked to set up a username and password. The root user will be disabled.

! After configuration, WiFi and Bluetooth are disabled. You can only update the software using physical access with
a cable using the ethernet port. The pusher is not a "smart" device :)


## Deploying the software over cable
You need: ethernet cable, machine with ethernet port, installed on the host: rsync, ssh and python3.

* The pusher uses a fixed ip: 100.66.6.42. Configure Mac OS to have IP 100.66.6.41 or something like it.
* Create a deploy_config.py file that looks like deploy_config.dist.py, set up the username and password as desired

To update the software:
`python3 deploy_over_ssh.py`

After this the software starts automatically. It will also startup on boot.

## Websocket architecture
This is my first project using websockets. They are not as easy as they seem.pusher
For this, channels and channels_redis are used. The backend, that carries the messages runs
on redis.

Website -> connects to django channels succesfully -> consumers.py
```
WebSocket HANDSHAKING /ws/pusher/ [127.0.0.1:49717]
INFO:django.channels.server:WebSocket HANDSHAKING /ws/pusher/ [127.0.0.1:49717]
DEBUG:daphne.http_protocol:Upgraded connection ['127.0.0.1', 49717] to WebSocket
DEBUG:aioredis:Creating tcp connection to ('127.0.0.1', 6379)
DEBUG:daphne.ws_protocol:WebSocket ['127.0.0.1', 49717] open and established
WebSocket CONNECT /ws/pusher/ [127.0.0.1:49717]
INFO:django.channels.server:WebSocket CONNECT /ws/pusher/ [127.0.0.1:49717]
DEBUG:daphne.ws_protocol:WebSocket ['127.0.0.1', 49717] accepted by application
DEBUG:daphne.ws_protocol:WebSocket incoming frame on ['127.0.0.1', 49717]
```

Several chat consumers can now talk to each other. So reloading a pusher page
also triggers an action on the other pusher page that is already open.

The only thing left to do is to be able to send a message to the same group and channel
that the consumers are listening to. Perhaps i need another consumer for that...


## Normalize your music
https://www.maketecheasier.com/normalize-music-files-with-ffmpeg/




# Manual inspection, debugging and such ==
You can log on to the pusher via SSH.

To stop the pusher software: `stop_pusher.sh`
To start the pusher software: `start_pusher.sh`

Todo: this will be a service.


# Developing game logic and website ==
`pip install -r requirements.txt`


To test/develop client software
```
python3 pusher_dev.py
```

